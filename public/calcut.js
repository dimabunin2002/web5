function click1() {
    let price = document.getElementById("price").value;
    let count = document.getElementById("count").value;
    let result = document.getElementById("result");
    result.value = count * price;
    if ((!Number(price) || (!Number(count))) || (count < 0) || (price < 0)) {
        result.value = "ошибка";
    }
    return;
}
